<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{

    public function display(...$path)
    {
        $this->viewBuilder()->setLayout('menu');
        $count = count($path);

        $this->loadModel('Products');
        $this->loadModel('Categories');
        // $categories = $this->Categories->find('all')
        // ->contain(
        //             ['Products'=>
        //                 ['conditions'=>
        //                     ['Products.is_available'=>1]
        //                 ]
        //             ]
        //         )
        // ->order('Categories.order asc')
        // ->limit(200);
        $categories = $this->Categories->find('all')
                                        ->contain('Products')
                                        ->order('Categories.order asc')
                                        ->limit(200);

        $promos = $this->Products->find('all')
                    ->where([
                            'Products.is_promo' => 1
                            ])
                    ->contain(['Categories'])
                    ->limit(200);
            
        $this->set(compact('promos', 'categories'));

        
        if (!$count) {
            return $this->redirect('/');
        }
        if (in_array('..', $path, true) || in_array('.', $path, true)) {
            throw new ForbiddenException();
        }
        $page = $subpage = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        
        $this->set(compact('page', 'subpage'));

        try {
            $this->render(implode('/', $path));
        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
    }
}
