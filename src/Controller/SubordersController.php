<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Suborders Controller
 *
 * @property \App\Model\Table\SubordersTable $Suborders
 *
 * @method \App\Model\Entity\Suborder[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SubordersController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Orders', 'Products']
        ];
        $suborders = $this->paginate($this->Suborders);

        $this->set(compact('suborders'));
    }

    /**
     * View method
     *
     * @param string|null $id Suborder id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $suborder = $this->Suborders->get($id, [
            'contain' => ['Orders', 'Products']
        ]);

        $this->set('suborder', $suborder);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($order_id=null)
    {
        if($order_id == null || !is_numeric($order_id)){
            $this->redirect(['controller'=>'Orders', 'action' => 'index']);
        }
        $suborder = $this->Suborders->newEntity();
        if ($this->request->is('post')) {
            $suborder = $this->Suborders->patchEntity($suborder, $this->request->getData());
            if ($this->Suborders->save($suborder)) {
                $this->Flash->success(__('New order has been saved.'));

                return $this->redirect(['controller'=>'Orders', 'action' => 'view', $order_id]);
            }
            $this->Flash->error(__('The suborder could not be saved. Please, try again.'));
        }
        $orders = $this->Suborders->Orders->find('list', ['limit' => 200]);
        $products = $this->Suborders->Products->find('list', ['limit' => 200]);
        $this->set(compact('suborder', 'orders', 'products', 'order_id'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Suborder id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null, $order_id=null)
    {

        $suborder = $this->Suborders->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $suborder = $this->Suborders->patchEntity($suborder, $this->request->getData());
            if ($this->Suborders->save($suborder)) {
                $this->Flash->success(__('The suborder has been saved.'));

                return $this->redirect(['controller'=>'Orders', 'action' => 'view', $order_id]);
            }
            $this->Flash->error(__('The suborder could not be saved. Please, try again.'));
        }
        $orders = $this->Suborders->Orders->find('list', ['limit' => 200]);
        $products = $this->Suborders->Products->find('list', ['limit' => 200]);
        $this->set(compact('suborder', 'orders', 'products', 'id'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Suborder id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $suborder = $this->Suborders->get($id);
        if ($this->Suborders->delete($suborder)) {
            $this->Flash->success(__('The suborder has been deleted.'));
        } else {
            $this->Flash->error(__('The suborder could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
