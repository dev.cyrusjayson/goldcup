<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use CakeDC\Users\Controller\Component\UsersAuthComponent;
/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');
        $this->loadComponent('CakeDC/Users.UsersAuth');
        //$this->Auth->config('logoutAction', ["controller"=>"Products", "action"=>"index"]);

        $this->Auth->config('loginRedirect', '/admins/login');
    }
    public function beforeFilter(Event $event)
    {
        $config['Auth']['authorize']['CakeDC/Users.SimpleRbac'] = [
            // autoload permissions.php
            'autoload_config' => 'permissions',
            // role field in the Users table
            'role_field' => 'role',
            // default role, used in new users registered and also as role matcher when no role is available
            'default_role' => 'user',
            [
                'role' => 'admin',
                'plugin'=>'*', //(optional, default = null)
                'prefix'=>'*', //(optional, default = null)
                'extension'=>'*', //(optional, default = null)
                'controller'=>'*',
                'action'=>'*',
                'allowed'=>true
            ],
            [
                'role' => '*',
                'plugin' => 'CakeDC/Users',
                'controller' => 'Users',
                'action' => ['profile','logout','login']
            ],
            [
                'role' => '*',
                'controller' => 'Admins',
                'action' => '*'
            ],
            // waiter
            [
                'role' => 'waiter',
                'controller' => 'Orders',
                'action' => ['add', 'edit', 'view', 'index']
            ],
            [
                'role' => 'waiter',
                'controller' => 'Suborders',
                'action' => ['add', 'edit', 'view', 'index']
            ],
            [
                'role' => 'waiter',
                'controller' => 'Customers',
                'action' => ['view', 'index']
            ],

            // kitchen
            [
                'role' => 'kitchen',
                'controller' => 'Orders',
                'action' => ['edit', 'view', 'index']
            ],
            [
                'role' => 'kitchen',
                'controller' => 'Suborders',
                'action' => ['edit', 'view', 'index']
            ],
            [
                'role' => 'kitchen',
                'controller' => 'Customers',
                'action' => ['view', 'index']
            ],

            // cashier
            [
                'role' => 'cashier',
                'controller' => 'Orders',
                'action' => ['edit', 'view', 'index']
            ],
            [
                'role' => 'cashier',
                'controller' => 'Suborders',
                'action' => ['edit', 'view', 'index']
            ],
            [
                'role' => 'cashier',
                'controller' => 'Customers',
                'action' => ['view', 'index']
            ],

            'permissions' => [], 
            'log' => true
            ];
            


         $this->Auth->allow(['view', 'display']);
    }

}
