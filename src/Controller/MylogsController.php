<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Orders Controller
 *
 * @property \App\Model\Table\OrdersTable $Orders
 *
 * @method \App\Model\Entity\Order[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MylogsController extends AppController
{
  
    public function beforeFilter(Event $event)
    {
        date_default_timezone_set('Asia/Manila');
    }
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $users = $this->paginate($this->Mylogs);

        $this->set(compact('users'));
    }
}