<?php
namespace App\Controller;

use App\Controller\AppController;
use CakeDC\Users\Controller\Component\UsersAuthComponent;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AdminsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */

    public function beforeFilter(Event $event)
    {
        date_default_timezone_set('Asia/Manila');
    }


    public function logout()
    {   
        $mylogs = TableRegistry::getTableLocator()->get('Mylogs');
        $mylog = $mylogs->find('all')->where(['sessionid' => $this->request->session()->id()])->first();
        
  
        $mylog->logout_date = date('Y-m-d H:i:s');
        $mylogs->save($mylog);
        $this->redirect('/logout');
    }

    public function login()
    {
  
        $mylogsTable = TableRegistry::getTableLocator()->get('Mylogs');
        $mylogs = $mylogsTable->newEntity();

        $mylogs->user_id = $this->getRequest()->getSession()->read('Auth.User.id');
        $mylogs->login_date = date('Y-m-d H:i:s');
        $mylogs->sessionid = $this->request->session()->id();

        if ($mylogsTable->save($mylogs)) {
            $id = $mylogs->id;
        }

        $this->redirect('/');
    }

}
