<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Order Entity
 *
 * @property int $id
 * @property string|null $name
 * @property int|null $customer_id
 * @property int|null $status_id
 * @property string|null $remarks
 * @property bool|null $is_paid
 * @property \Cake\I18n\FrozenTime|null $date_created
 *
 * @property \App\Model\Entity\Customer $customer
 * @property \App\Model\Entity\Status $status
 * @property \App\Model\Entity\Suborder[] $suborders
 */
class Order extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'name' => true,
        'customer_id' => true,
        'status_id' => true,
        'remarks' => true,
        'is_paid' => true,
        'date_created' => true,
        'customer' => true,
        'status' => true,
        'suborders' => true
    ];
}
