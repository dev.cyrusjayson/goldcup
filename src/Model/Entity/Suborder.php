<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Suborder Entity
 *
 * @property int $id
 * @property int|null $order_id
 * @property int|null $product_id
 * @property int|null $quantity
 * @property string|null $remarks
 * @property string|null $kitchen_remarks
 * @property bool|null $is_cancel
 * @property \Cake\I18n\FrozenTime|null $date_created
 *
 * @property \App\Model\Entity\Order $order
 * @property \App\Model\Entity\Product $product
 */
class Suborder extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'order_id' => true,
        'product_id' => true,
        'quantity' => true,
        'remarks' => true,
        'kitchen_remarks' => true,
        'is_cancel' => true,
        'date_created' => true,
        'order' => true,
        'product' => true
    ];
}
