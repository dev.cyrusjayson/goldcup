<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Product Entity
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $image
 * @property string|null $details
 * @property int|null $category_id
 * @property float|null $price
 * @property bool|null $is_available
 * @property \Cake\I18n\FrozenTime|null $date_created
 * @property bool|null $is_promo
 *
 * @property \App\Model\Entity\Category $category
 * @property \App\Model\Entity\Order[] $orders
 */
class Product extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'image' => true,
        'details' => true,
        'category_id' => true,
        'price' => true,
        'is_available' => true,
        'date_created' => true,
        'is_promo' => true,
        'category' => true,
        'orders' => true
    ];
}
