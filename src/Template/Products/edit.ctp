<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Product $product
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Related Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $product->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $product->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Products'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Categories'), ['controller' => 'Categories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Category'), ['controller' => 'Categories', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Orders'), ['controller' => 'Orders', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Order'), ['controller' => 'Orders', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="products form large-9 medium-8 columns content">
<?= $this->Flash->render() ?>
    <?= $this->Form->create($product) ?>
    <fieldset>
        <legend><?= __('Edit Product') ?></legend>
        <?php
            echo $this->Form->control('name', ['required' => true]);
            echo $this->Form->control('image', ['label'=>'Image URL', 'type'=>'text', 'required' => true]);
            echo $this->Form->control('details', ['type'=>'textarea', 'required' => true]);
            echo $this->Form->control('category_id', ['options' => $categories, 'empty' => true, 'required' => true]);
            echo $this->Form->control('price', ['type'=>'number', 'required' => true]);
            echo $this->Form->control('is_available', ['label'=>'Available']);
            echo $this->Form->control('is_promo');
        ?>
    </fieldset>
    <br>
    <?= $this->Form->button(__('Submit'), ['class'=>'btn btn-lg btn-warning']); ?>
    <?= $this->Form->end() ?>
</div>
