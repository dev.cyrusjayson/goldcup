<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Product[]|\Cake\Collection\CollectionInterface $products
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Related Actions') ?></li>
        <li><?= $this->Html->link(__('New Product'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Categories'), ['controller' => 'Categories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Category'), ['controller' => 'Categories', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Orders'), ['controller' => 'Orders', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Order'), ['controller' => 'Orders', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="products index large-9 medium-8 columns content">


<?= $this->Flash->render() ?>
    <?= $this->Html->link( '<i class="fas fa-plus"></i> Add product',
                        ['action' => 'add'],
                        ['escape' => false, 'class'=>'btn btn-primary float-right', 'title'=>'Add Product']) ?>
    <h3><?= __('Products') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('image') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('details') ?></th>
                <th scope="col"><?= $this->Paginator->sort('category_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('price') ?></th>
                <th scope="col"><?= $this->Paginator->sort('is_available', 'Available') ?></th>
                <th scope="col"><?= $this->Paginator->sort('is_promo', 'Promo') ?></th>
                <th scope="col" class="actions">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($products as $product): ?>
            <tr>
                <td>
                    <a href="<?= h($product->image) ?>" target="_blank">
                        <img src="<?= h($product->image) ?>" class="rounded img-thumbnail" style="height:50px;">
                    </a>
                </td>
                <td><?= h($product->name) ?></td>
                <td><?= h($product->details) ?></td>
                <td><?= $product->has('category') ? $this->Html->link($product->category->name, ['controller' => 'Categories', 'action' => 'view', $product->category->id]) : '' ?></td>
                <td><?= $this->Number->format($product->price) ?></td>
                <td><?= ($product->is_available) ? '<span class="badge badge-success">Yes</span>':'<span class="badge badge-danger">No</span>' ?></td>
                <td><?= ($product->is_promo) ? '<span class="badge badge-success">Yes</span>':'<span class="badge badge-secondary">No</span>' ?></td>
                <td class="text-right">
                    <?= $this->Html->link( '<i class="fas fa-info-circle"></i>',
                                            ['action' => 'view', $product->id],
                                            ['escape' => false]) ?> &nbsp;
                    <?= $this->Html->link( '<i class="fas fa-edit"></i>',
                                            ['action' => 'edit', $product->id],
                                            ['escape' => false]) ?> &nbsp;
                    <?= $this->Form->postLink(__('<i class="fas fa-trash"></i>'), ['action' => 'delete', $product->id], ['escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $product->id)]) ?> 
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
