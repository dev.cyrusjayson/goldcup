<?php 
    $product_names = array_map(function ($ar) {return $ar['name'];}, $products->toArray());
    $total = array();
    foreach($products as $product){
        (isset($product->suborders[0])) ? array_push($total, $product->suborders[0]->total) : array_push($total, 0);
    }
    $product_names = json_encode($product_names); 
    $total = json_encode($total); 
?>

<div class="container" style="padding-top:50px;">

<div class="row">
    <div class="col-lg-5">
       
        <div class="input-group mb-3">
                <input type="text" id="date" class="form-control" placeholder="Pick date" value="<?php echo $date_filter; ?>">
                <div class="input-group-append">
                    <button class="btn btn-outline-secondary" type="button" id="btn-search">Search</button>
                </div>
            </form>
        </div>


    </div>
</div>



<script>
$(function(){
    $('#date').datepicker({format: "yyyy-mm-dd"});
    $('#btn-search').click(function() {
        if($('#date').val().length > 0){
            window.location = "/products/sales?date=" + $('#date').val();
        }else{
            //alert('Please pick your date');
            $('#myModal').modal();
        }
        
    });
})
</script>


<canvas id="myChart" width="300" height="100"></canvas>
<script>
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: <?php echo $product_names; ?>,
        datasets: [{
            label: 'Sales of <?php echo $date_filter; ?>',
            data: <?php echo $total; ?>,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
</script>



<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Please select your desired date.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Okay</button>
      </div>
    </div>
  </div>
</div>


</div>
