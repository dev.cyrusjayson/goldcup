
<div class="container">
    <div class="suborders index columns content">

        <?= $this->Flash->render() ?>

        <h3><?= __('Foods') ?></h3>
        <table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('order_id') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('product_id') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('quantity') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('is_cancel') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('date_created') ?></th>
                    <th scope="col" class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($suborders as $suborder): ?>
                <tr>
                    <td><?= $this->Number->format($suborder->id) ?></td>
                    <td><?= $suborder->has('order') ? $this->Html->link($suborder->order->id, ['controller' => 'Orders', 'action' => 'view', $suborder->order->id]) : '' ?></td>
                    <td><?= $suborder->has('product') ? $this->Html->link($suborder->product->name, ['controller' => 'Products', 'action' => 'view', $suborder->product->id]) : '' ?></td>
                    <td><?= $this->Number->format($suborder->quantity) ?></td>
                    <td><?= h($suborder->is_cancel) ?></td>
                    <td><?= h($suborder->date_created) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $suborder->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $suborder->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $suborder->id], ['confirm' => __('Are you sure you want to delete # {0}?', $suborder->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->first('<< ' . __('first')) ?>
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
                <?= $this->Paginator->last(__('last') . ' >>') ?>
            </ul>
            <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
        </div>
    </div>
</div>
