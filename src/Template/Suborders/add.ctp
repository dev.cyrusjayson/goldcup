<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Suborder $suborder
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Suborders'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Orders'), ['controller' => 'Orders', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Order'), ['controller' => 'Orders', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="suborders form large-9 medium-8 columns content">
<?= $this->Flash->render() ?>

    <?= $this->Form->create($suborder) ?>
    <fieldset>
        <legend><?= __('Add Food') ?></legend>
        <?php
            echo $this->Form->control('order_id', ['value' => $order_id, 'type' => 'hidden']);
            echo $this->Form->control('product_id', ['options' => $products, 'empty' => true, 'required'=>true]);
            echo $this->Form->control('quantity', ['value' => 1, 'required'=>true]);
            echo $this->Form->control('remarks');
            //echo $this->Form->control('kitchen_remarks');
            //echo $this->Form->control('is_cancel');
        ?>
    </fieldset>
    <br>
    <?= $this->Form->button(__('Submit'), ['class'=>'btn btn-lg btn-warning']); ?>
    <?= $this->Form->end() ?>
</div>
