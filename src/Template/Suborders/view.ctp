<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Suborder $suborder
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Suborder'), ['action' => 'edit', $suborder->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Suborder'), ['action' => 'delete', $suborder->id], ['confirm' => __('Are you sure you want to delete # {0}?', $suborder->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Suborders'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Suborder'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Orders'), ['controller' => 'Orders', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Order'), ['controller' => 'Orders', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="suborders view large-9 medium-8 columns content">

    <?= $this->Flash->render() ?>
    <h3>Details</h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Order') ?></th>
            <td><?= $suborder->has('order') ? $this->Html->link($suborder->order->id, ['controller' => 'Orders', 'action' => 'view', $suborder->order->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Product') ?></th>
            <td><?= $suborder->has('product') ? $this->Html->link($suborder->product->name, ['controller' => 'Products', 'action' => 'view', $suborder->product->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($suborder->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date Created') ?></th>
            <td><?= h($suborder->date_created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Cancel') ?></th>
            <td><?= $suborder->is_cancel ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Cancel') ?></th>
            <td><?= $suborder->is_cancel ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <h4><?= __('Remarks') ?></h4>
    <div class="row">
        <div class="container">
        <?= $this->Text->autoParagraph(h($suborder->remarks)); ?>
        </div>
    </div>
    <h4><?= __('Kitchen Remarks') ?></h4>
    <div class="row">
        <div class="container">
        <?= $this->Text->autoParagraph(h($suborder->kitchen_remarks)); ?>
        </div>
    </div>
</div>
