<div class="container">
    <div class="suborders form  columns content">
        <?= $this->Flash->render() ?>
        <?= $this->Form->create($suborder) ?>
        <fieldset>
            <legend><?= __('Edit Food') ?></legend>
            <?php
                //echo $this->Form->control('order_id', ['value' => $id, 'type' => 'hidden']);
                //echo $this->Form->control('product_id', ['options' => $products, 'empty' => true, 'required'=>true]);
                //echo $this->Form->hidden('order_id', ['value'=>$id]);
                echo $this->Form->control('order_id', ['options' => $orders, 'empty' => true, 'disabled'=>true]);
                echo $this->Form->control('product_id', ['options' => $products, 'empty' => true]);
                echo $this->Form->control('quantity', ['required'=>true]);
                echo $this->Form->control('remarks');
                echo $this->Form->control('kitchen_remarks');
                echo $this->Form->control('is_cancel', ['label'=>'Cancel order']);
            ?>
        </fieldset>
        <br>

        
        <?= $this->Form->button(__('Submit'), ['class'=>'btn btn-lg btn-warning']); ?>
        <?= $this->Form->end() ?>


        <?php 
            $session = $this->getRequest()->getSession();
            if($session->read('Auth.User.role') !== 'admin' || $session->read('Auth.User.role') !== 'superuser'){ ?>
            <?= $this->Form->postLink(
                    __('Delete'),
                    ['action' => 'delete', $suborder->id],
                    ['confirm' => __('Are you sure you want to delete # {0}?', $suborder->id)]
                )
            ?>
        <?php } ?>

    </div>
</div>

