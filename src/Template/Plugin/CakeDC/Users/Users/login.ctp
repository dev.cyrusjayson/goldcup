<?php
use Cake\Core\Configure;
$this->layout = false;
?>

<?php
$cakeDescription = 'Goldcup';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>: Login
    </title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    
    <script src="https://kit.fontawesome.com/11838118b2.js" crossorigin="anonymous"></script>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</head>
<body>

    <div class="container-fluid">

<style>
  html,
body {
  height: 100%;
}

body {
  display: -ms-flexbox;
  display: flex;
  -ms-flex-align: center;
  align-items: center;
  padding-top: 40px;
  padding-bottom: 40px;
  background-color: #f5f5f5;
}

form {
  width: 100%;
  max-width: 330px;
  padding: 15px;
  margin: auto;
}
form .checkbox {
  font-weight: 400;
}
form .form-control {
  position: relative;
  box-sizing: border-box;
  height: auto;
  padding: 10px;
  font-size: 16px;
}
form .form-control:focus {
  z-index: 2;
}
form input[type="email"] {
  margin-bottom: -1px;
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0;
}
form input[type="password"] {
  margin-bottom: 10px;
  border-top-left-radius: 0;
  border-top-right-radius: 0;
}


</style>






    <?= $this->Form->create() ?>
    <?= $this->Flash->render('auth') ?>
      <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
      <?= $this->Form->control('username', ['placeholder'=>'Username', 'class'=>'form-control', 'type'=>'text', 'required' => true]) ?>
      <?= $this->Form->control('password', ['placeholder'=>'Password', 'class'=>'form-control', 'type'=>'password', 'required' => true]) ?>
       
   
      <div class="checkbox mb-3">
      <label>
        <?php
        if (Configure::read('Users.RememberMe.active')) {
            echo $this->Form->control(Configure::read('Users.Key.Data.rememberMe'), [
                'type' => 'checkbox',
                'label' => __d('CakeDC/Users', 'Remember me'),
                'checked' => Configure::read('Users.RememberMe.checked')
            ]);
        }
        ?>
      </label>
    </div>
    
    <?= $this->Form->button(__d('CakeDC/Users', 'Sign in'), ['class'=>'btn btn-lg btn-primary btn-block']); ?>
    <?= $this->Form->end() ?>



    </div>
</body>
</html>