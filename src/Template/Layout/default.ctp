<?php
$cakeDescription = 'Goldcup';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>: 
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('style.css') ?>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
     
    <script src="https://kit.fontawesome.com/11838118b2.js" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.1/dist/Chart.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
    
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

</head>
<body>
    <nav class="top-bar expanded" data-topbar role="navigation">
        <ul class="title-area large-3 medium-4 columns">
            <li class="name">
                <h1><a href=""><?= $this->fetch('title') ?></a></h1>
            </li>
        </ul>
        <div class="top-bar-section">

            <ul class="nav nav-pills float-left">
                <li><a href="#">Account: <?= ucfirst($this->getRequest()->getSession()->read('Auth.User.role')); ?></a></li>
            </ul>
            <ul class="nav nav-pills float-right">
                <li><a href="<?php echo $this->Url->build('/', true); ?>">Home</a></li>
                <li><a href="<?php echo $this->Url->build('/customers', true); ?>">Customers</a></li>
                <li><a href="<?php echo $this->Url->build('/orders', true); ?>">Orders</a></li>

                <?php
                $session = $this->getRequest()->getSession();
                if($session->read('Auth.User.role') == 'cashier' || $session->read('Auth.User.role') == 'admin' || $session->read('Auth.User.role') == 'superuser'){ ?>
                    <li><a href="<?php echo $this->Url->build('/products/sales', true); ?>">Sales</a></li>
                <?php } ?>
                <?php
                $session = $this->getRequest()->getSession();
                if($session->read('Auth.User.role') == 'admin' || $session->read('Auth.User.role') == 'superuser'){ ?>
                    <li><a href="<?php echo $this->Url->build('/products', true); ?>">Products</a></li>
                    <li><a href="<?php echo $this->Url->build('/categories', true); ?>">Categories</a></li>
                    <li><a href="<?php echo $this->Url->build('/statuses', true); ?>">Status</a></li>
                    <li><a href="<?php echo $this->Url->build('/users/users', true); ?>">Administraitor</a></li>
                    <li><a href="<?php echo $this->Url->build('/mylogs', true); ?>">Logs</a></li>
                <?php } ?>
                <li><a href="<?php echo $this->Url->build('/admins/logout', true); ?>">Logout</a></li>            
            </ul>


        
        </div>
    </nav>
    <div class="container-fluid">
        <?= $this->fetch('content') ?>
    </div>
</body>
</html>
