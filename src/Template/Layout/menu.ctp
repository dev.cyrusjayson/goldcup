<?php $them_path = "/theme/"; ?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Goldcup Specialty Coffee</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Free Template by Free-Template.co" />
    <meta name="keywords" content="free bootstrap 4, free bootstrap 4 template, free website templates, free html5, free template, free website template, html5, css3, mobile first, responsive" />
    <meta name="author" content="Free-Template.co" />
    
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700|Raleway" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo $them_path; ?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo $them_path; ?>css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo $them_path; ?>css/animate.css">
    
    <link rel="stylesheet" href="<?php echo $them_path; ?>css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo $them_path; ?>css/owl.theme.default.min.css">
    <!-- <link rel="stylesheet" href="<?php echo $them_path; ?>css/magnific-popup.css"> -->


    

    <link rel="stylesheet" href="<?php echo $them_path; ?>css/icomoon.css">
    <link rel="stylesheet" href="<?php echo $them_path; ?>css/style.css">
  </head>
  <body data-spy="scroll" data-target="#ftco-navbar" data-offset="200">
    
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
      <div class="container">
        <a class="navbar-brand" href="index.html">&nbsp;</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="oi oi-menu"></span> Menu
        </button>

        <div class="collapse navbar-collapse" id="ftco-nav">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active"><a href="#section-home" class="nav-link">Home</a></li>
            <li class="nav-item"><a href="#section-offer" class="nav-link">Offer</a></li>
            <li class="nav-item"><a href="#section-menu" class="nav-link">Menu</a></li>
            <li class="nav-item"><?= $this->Html->link(__('Crew'), ['controller'=>'Orders', 'action' => 'index'],['class'=>'nav-link']) ?></li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- END nav -->


    <?= $this->fetch('content') ?>

    <!-- loader -->
    <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>
    
    <script src="<?php echo $them_path; ?>js/jquery.min.js"></script>
    <script src="<?php echo $them_path; ?>js/popper.min.js"></script>
    <script src="<?php echo $them_path; ?>js/bootstrap.min.js"></script>
    <script src="<?php echo $them_path; ?>js/jquery.easing.1.3.js"></script>
    <script src="<?php echo $them_path; ?>js/jquery.waypoints.min.js"></script>
    <script src="<?php echo $them_path; ?>js/owl.carousel.min.js"></script>
    <!-- <script src="<?php echo $them_path; ?>js/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo $them_path; ?>js/jquery.animateNumber.min.js"></script> -->
    <script src="<?php echo $them_path; ?>js/main.js"></script>    
  </body>
</html>