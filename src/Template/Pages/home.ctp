<?php $them_path = "/theme/"; ?>
<section class="ftco-cover" style="background-image: url(<?php echo $them_path; ?>images/bg_3.jpg);" id="section-home">
    <div class="container">
    <div class="row align-items-center justify-content-center text-center ftco-vh-100">
        <div class="col-md-12">
        <h1 class="ftco-heading ftco-animate mb-3">
            Welcome To<br>
            Goldcup Specialty Coffee
        </h1>
        </div>
    </div>
    </div>
</section>
<!-- END section -->



<section class="ftco-section bg-light" id="section-offer">
    <div class="container">
    
    <div class="row">
        <div class="col-md-12 text-center mb-5 ftco-animate">
        <h4 class="ftco-sub-title">Our Offers</h4>
        <h2 class="display-4">Offers &amp; Promos</h2>
        <div class="row justify-content-center">
            <div class="col-md-7">
            <p class="lead">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
            </div>
        </div>
        </div>
        <div class="col-md-12">
        <div class="owl-carousel ftco-owl">
            <?php foreach($promos as $promo){ ?>
                <div class="item">
                    <div class="media d-block mb-4 text-center ftco-media ftco-animate border-0">
                        <img src="<?php echo $promo['image']; ?>" alt="<?php echo $promo['name']; ?>" class="img-fluid">
                        <div class="media-body p-md-5 p-4">
                        <h5 class="text-primary">Php <?php echo $promo['price']; ?></h5>
                        <h5 class="mt-0 h4"><?php echo $promo['name']; ?></h5>
                        <p class="mb-4"><?php echo $promo['details']; ?></p>
                        </div>
                    </div>
                </div>
            <?php } ?>

        </div>
        </div>
        
    </div>
    </div>
</section>
<!-- END section -->

<section class="ftco-section" id="section-menu">
    <div class="container">
    <div class="row">
        <div class="col-md-12 text-center mb-5 ftco-animate">
        <h2 class="display-4">Today's Menu</h2>
        <div class="row justify-content-center">
            <div class="col-md-7">
            <p class="lead">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
            </div>
        </div>
        </div>

        <div class="col-md-12 text-center">

        <ul class="nav ftco-tab-nav nav-pills mb-5" id="pills-tab" role="tablist">
            <?php $activeTabMenu = false; ?>
            <?php foreach($categories as $category){ ?>
                <li class="nav-item ftco-animate">
                    <a class="nav-link <?php echo (!$activeTabMenu) ? 'active': ''; ?>" id="pills-<?php echo $category['code']; ?>-tab" data-toggle="pill" href="#pills-<?php echo $category['code']; ?>" role="tab" aria-controls="pills-<?php echo $category['code']; ?>" aria-selected="true">
                        <?php echo $category['name']; ?>
                    </a>
                </li>
                <?php $activeTabMenu = true; ?>
            <?php } ?>
        </ul>

        <div class="tab-content text-left">
            <?php $activeTabBody = false; ?>
            <?php foreach($categories as $category){ ?>
                <div class="tab-pane fade show <?php echo (!$activeTabBody) ? 'active': ''; ?>" id="pills-<?php echo $category['code']; ?>" role="tabpanel" aria-labelledby="pills-<?php echo $category['code']; ?>-tab">
                    <div class="row">

                        <?php foreach($category['products'] as $product){ ?>
                            <div class="col-md-12 ftco-animate">
                                    <div class="media menu-item">
                                        <img class="mr-3" src="<?php echo $product['image']; ?>" class="img-fluid" alt="<?php echo $product['name']; ?>">
                                        <div class="media-body">
                                            <h5 class="mt-0"><?php echo $product['name']; ?> <?php echo (!$product['is_available']) ? '<span class="badge badge-danger">Not Available</span>': ''; ?></h5>
                                            <p><?php echo $product['details']; ?></p>
                                            <h6 class="text-primary menu-price">Php <?php echo $product['price']; ?></h6>
                                        </div>
                                    </div>
                            </div>
                        <?php } ?>

                    </div>
                </div>
                <?php $activeTabBody = true; ?>
            <?php } ?>
        </div>

        </div>
    </div>
    </div>
</section>
<!-- END section -->

