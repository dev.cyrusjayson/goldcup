

<div class="container">

    <div class="orders index columns content">
        <?= $this->Flash->render() ?>

        <?= $this->Html->link( '<i class="fas fa-plus"></i> New order',
                                ['action' => 'add'],
                                ['escape' => false, 'class'=>'btn btn-primary float-right', 'title'=>'New Order']) ?>
                        

        <h3><?= __('Orders') ?></h3>

        <div class="realtime-wrapper">
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th scope="col"><?= $this->Paginator->sort('user_id', 'Employee') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('customer_id', 'Table') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('status_id') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('is_paid', 'Payment') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('date_created') ?></th>
                        <th scope="col" class="actions text-right"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($orders as $order): ?>
                    <tr>
                        <td><?= ($order->user) ? $order->user->username : '' ?></td>
                        <td>
                            <a href="<?php echo $this->Url->build('/orders/view', true); ?>/<?= h($order->id) ?>">
                                <?= h($order->name) ?>
                            </a>
                            
                        </td>
                        <td><?= $order->has('customer') ? $this->Html->link($order->customer->name, ['controller' => 'Customers', 'action' => 'view', $order->customer->id]) : '' ?></td>
                        <td><?= $order->has('status') ? $this->Html->link($order->status->name, ['controller' => 'Statuses', 'action' => 'view', $order->status->id]) : '' ?></td>
                        <td><?= ($order->is_paid) ? '<span class="badge badge-primary">Paid</span>':'<span class="badge badge-danger">Pending</span>' ?></td>
                        <td><?= h($order->date_created) ?></td>
                        <td class="text-right">
                            <?= $this->Html->link( '<i class="fas fa-info-circle"></i>',
                                                    ['action' => 'view', $order->id],
                                                    ['escape' => false]) ?> &nbsp;
                            <?= $this->Html->link( '<i class="fas fa-edit"></i>',
                                                    ['action' => 'edit', $order->id],
                                                    ['escape' => false]) ?> 
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('first')) ?>
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                    <?= $this->Paginator->last(__('last') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
            </div>
        </div>
    </div>
</div>
<script>

$(function(){
    var temp = false;
setInterval(function(){ 
    console.log('thick');
    if(temp){
        return;
    }

    console.log('thick http');
    temp = $.ajax({
              type:'get',
              url: '/orders/updates',
			  dataType: 'html',
			  success:function(result){
                temp = false;
				$('.realtime-wrapper').html(result);
			  }
		  });

 }, 1000);
})


</script>