<?php 
        $total = 0;
        foreach ($order->suborders as $suborders){
            if(!$suborders->is_cancel){
                $total = $total + ($suborders->product->price * $suborders->quantity);
            }
        }
    ?>  

<div class="container">
    <div class="orders view  columns content">
        <div class="clear clearfix">
            <?= $this->Flash->render() ?>
        </div>

        <?= $this->Html->link( '<i class="fas fa-plus"></i> Add food',
                                    ['controller' => 'Suborders', 'action' => 'add', $order->id],
                                    ['escape' => false, 'class'=>'btn btn-primary float-right', 'title'=>'New Order']) ?>
<div class="clear clearfix"></div>
        <h4>Total: Php <?php echo number_format($total); ?></h4>
        <br>

        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Details</a>
                <a class="nav-item nav-link active" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Foods</a>
            </div>
        </nav>
        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                <table class="table">
                    <tr>
                        <th scope="row"><?= __('Id') ?></th>
                        <td><?= $this->Number->format($order->id) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Name') ?></th>
                        <td><?= h($order->name) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Table') ?></th>
                        <td><?= $order->has('customer') ? $this->Html->link($order->customer->name, ['controller' => 'Customers', 'action' => 'view', $order->customer->id]) : '' ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Status') ?></th>
                        <td><?= $order->has('status') ? $this->Html->link($order->status->name, ['controller' => 'Statuses', 'action' => 'view', $order->status->id]) : '' ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Payment') ?></th>
                        <td><?= ($order->is_paid) ? '<span class="badge badge-success">Yes</span>':'<span class="badge badge-danger">No</span>' ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Date Created') ?></th>
                        <td><?= h($order->date_created) ?></td>
                    </tr>
                </table>
                
                <h4><?= __('Remarks') ?></h4>
                <div class="row">
                    <div class="container">
                        <?= $this->Text->autoParagraph(h($order->remarks)); ?>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade show active" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">

            <div class="related">
                <br>
                    <?php if (!empty($order->suborders)): ?>
                        <?php foreach ($order->suborders as $suborders): ?>

                        <div class="media">
                            <img src="<?= h($suborders->product->image) ?>" class="mr-3 rounded img-thumbnail" style="width:70px;">
                            <div class="media-body">
                                <h5 class="mt-0"> <?= h($suborders->product->name) ?></h5>
                                <p><?= h($suborders->product->details) ?></p>
                                <b>Price:</b>  Php <?= h($suborders->product->price) ?>

                                <br><b>Quantity:</b> <?= h($suborders->quantity) ?>

                                <br><b>Remarks:</b> <?= h($suborders->remarks) ?>
                                
                                <br><b>Kitchen Remarks:</b> <?= h($suborders->kitchen_remarks) ?>

                                <br><b>Status:</b> <?php echo ($suborders->is_cancel) ? '<span class="badge badge-danger">Cancel</span>':'<span class="badge badge-light">OK</span>'; ?>
                                
                                <br><b>Date:</b><?= h($suborders->date_created) ?>
                                <br>  <br>
                                <?php
                                    echo $this->Html->link(
                                        'Edit',
                                        '/suborders/edit/'.$suborders->id . '/' . $order->id,
                                        ['class' => 'btn btn-warning btn-sm']
                                    );
                                ?>
                                <br><br>
                            </div>
                        </div>

                        
                    
                        <?php endforeach; ?>
                    
                    <?php endif; ?>
                </div>
                
            </div>
        </div>
    </div>

</div>
