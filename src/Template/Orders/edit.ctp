<div class="container">
    <div class="orders form columns content">
        <?= $this->Flash->render() ?>
        <?= $this->Form->create($order) ?>
        <fieldset>
            <legend><?= __('Edit Order') ?></legend>
            <?php
                $session = $this->getRequest()->getSession();
                if($session->read('Auth.User.role') !== 'cashier' && $session->read('Auth.User.role') !== 'kitchen'){
                    echo $this->Form->control('name');
                    echo $this->Form->control('customer_id', ['options' => $customers, 'empty' => true]);
                }

                echo $this->Form->control('status_id', ['options' => $statuses, 'empty' => true]);

                if($session->read('Auth.User.role') !== 'cashier'){
                    echo $this->Form->control('remarks');
                }

                if($session->read('Auth.User.role') !== 'waiter' && $session->read('Auth.User.role') !== 'kitchen'){
                    echo "<br>" .$this->Form->control('is_paid', ['label'=>'Paid. Check to mark paid']);
                }
            ?>
        </fieldset>
        <br>
        <?= $this->Form->button(__('Submit'), ['class'=>'btn btn-lg btn-warning']); ?>
        <?= $this->Form->end() ?>
    </div>

</div>
