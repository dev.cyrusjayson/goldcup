<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Order $order
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Orders'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Customers'), ['controller' => 'Customers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Customer'), ['controller' => 'Customers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Statuses'), ['controller' => 'Statuses', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Status'), ['controller' => 'Statuses', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Suborders'), ['controller' => 'Suborders', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Suborder'), ['controller' => 'Suborders', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="orders form large-9 medium-8 columns content">
<?= $this->Flash->render() ?>
    <?= $this->Form->create($order) ?>
    <fieldset>
        <legend><?= __('Add Order') ?></legend>
        <?php
            echo $this->Form->control('user_id', ['type'=>'hidden', 'value'=>$this->getRequest()->getSession()->read('Auth.User.id')]);
            echo $this->Form->control('name');
            echo $this->Form->control('customer_id', ['options' => $customers, 'empty' => true]);
            echo $this->Form->control('status_id', ['options' => $statuses, 'empty' => true]);
            echo $this->Form->control('remarks');
            //echo $this->Form->control('is_paid');
        ?>
    </fieldset>
    <br>
    <?= $this->Form->button(__('Submit'), ['class'=>'btn btn-lg btn-warning']); ?>
    <?= $this->Form->end() ?>
</div>
