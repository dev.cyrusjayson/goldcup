<?php
return [
    'CakeDC/Auth.permissions' => [
        //admin role allowed to all the things
        [
            'role' => 'admin',
            'prefix' => '*',
            'extension' => '*',
            'plugin' => '*',
            'controller' => '*',
            'action' => '*',
        ],
        //specific actions allowed for the all roles in Users plugin
        [
            'role' => '*',
            'plugin' => 'CakeDC/Users',
            'controller' => 'Users',
            'action' => ['profile', 'logout', 'login'],
        ],
        [
            'role' => '*',
            'controller' => 'Admins',
            'action' => '*',
        ],

        // waiter
        [
            'role' => 'waiter',
            'controller' => 'Orders',
            'action' => ['add', 'edit', 'view', 'index'],
        ],
        [
            'role' => 'waiter',
            'controller' => 'Suborders',
            'action' => ['add', 'edit', 'view', 'index']
        ],
        [
            'role' => 'waiter',
            'controller' => 'Customers',
            'action' => ['view', 'index']
        ],

        // kitchen
        [
            'role' => 'kitchen',
            'controller' => 'Orders',
            'action' => ['edit', 'view', 'index']
        ],
        [
            'role' => 'kitchen',
            'controller' => 'Suborders',
            'action' => ['edit', 'view', 'index']
        ],
        [
            'role' => 'kitchen',
            'controller' => 'Customers',
            'action' => ['view', 'index']
        ],

        // cashier
        [
            'role' => 'cashier',
            'controller' => 'Orders',
            'action' => ['edit', 'view', 'index']
        ],
        [
            'role' => 'cashier',
            'controller' => 'Suborders',
            'action' => ['edit', 'view', 'index']
        ],
        [
            'role' => 'cashier',
            'controller' => 'Customers',
            'action' => ['view', 'index']
        ],
        //all roles allowed to Pages/display
        [
            'role' => '*',
            'controller' => 'Pages',
            'action' => 'display',
        ],
        [
            'role' => '*',
            'controller' => 'Orders',
            'action' => 'updates',
        ],
    ]
];
?>
