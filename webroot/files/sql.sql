-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Oct 29, 2019 at 12:52 AM
-- Server version: 5.7.26
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `goldcup`
--

-- --------------------------------------------------------

--
-- Table structure for table `cake_d_c_users_phinxlog`
--

CREATE TABLE `cake_d_c_users_phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cake_d_c_users_phinxlog`
--

INSERT INTO `cake_d_c_users_phinxlog` (`version`, `migration_name`, `start_time`, `end_time`, `breakpoint`) VALUES
(20150513201111, 'Initial', '2019-10-26 14:08:17', '2019-10-26 14:08:17', 0),
(20161031101316, 'AddSecretToUsers', '2019-10-26 14:08:17', '2019-10-26 14:08:17', 0),
(20190208174112, 'AddAdditionalDataToUsers', '2019-10-26 14:08:17', '2019-10-26 14:08:17', 0);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `order` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `code`, `order`) VALUES
(1, 'Breakfast', 'breakfast', 1),
(2, 'Lunch', 'lunch', 2),
(3, 'Dinner', 'dinner', 3),
(4, 'Coffee', 'coffee', 4),
(5, 'Drinks', 'drinks', 5),
(6, 'Dessert', 'dessert', 7),
(7, 'Cake', 'cake', 8),
(8, 'Snacks', 'snacks', 9),
(9, 'Tea', 'tea', 6);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_updated` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `date_updated`) VALUES
(1, 'Table 1', '2019-10-24 00:33:00'),
(2, 'Table 2', '2019-10-24 00:33:12'),
(3, 'Table 3', '2019-10-24 00:33:12'),
(4, 'Table 4', '2019-10-24 00:33:12'),
(5, 'Table 5', '2019-10-24 00:33:12'),
(6, 'Table 6', '2019-10-24 00:33:12'),
(7, 'Table 7', '2019-10-24 00:33:12'),
(8, 'Table 8', '2019-10-24 00:33:12'),
(9, 'Table 9', '2019-10-24 00:33:12'),
(10, 'Table 10', '2019-10-24 00:33:12');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL COMMENT 'Notify Kitchen\\nKitchen Accepted\\nProcessing\\nReady to serve\\nPending payment',
  `remarks` text,
  `is_paid` tinyint(1) DEFAULT '0',
  `date_created` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `name`, `customer_id`, `status_id`, `remarks`, `is_paid`, `date_created`) VALUES
(7, 'table 1 customer', 1, 2, '', 1, '2019-10-26 23:57:27'),
(8, 'table 2 customer', 5, 4, '', 1, '2019-10-26 23:57:42'),
(9, 'ciara', 8, 6, 'serve 30 minutes', 1, '2019-10-27 02:25:15'),
(10, 'jh', 6, NULL, '', 0, '2019-10-27 02:32:11'),
(11, 'Paul', 1, 4, '', 0, '2019-10-27 04:31:57');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `image` text,
  `details` varchar(45) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `is_available` tinyint(1) DEFAULT '1',
  `date_created` datetime DEFAULT CURRENT_TIMESTAMP,
  `is_promo` tinyint(1) DEFAULT '0',
  `photo` varchar(255) DEFAULT NULL,
  `dir` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `image`, `details`, `category_id`, `price`, `is_available`, `date_created`, `is_promo`, `photo`, `dir`) VALUES
(1, 'Salad', 'https://www.fifteenspatulas.com/wp-content/uploads/2011/10/Caesar-Salad-Fifteen-Spatulas-3.jpg', 'Salad details here\r\nsdfsf\r\n\r\nsdfsdf\r\nsdff', 1, 100, 0, '2019-10-24 00:31:41', 1, NULL, NULL),
(2, 'Coffee', 'https://img.etimg.com/thumb/msid-66650613,width-643,imgsize-801611,resizemode-4/coffee.jpg', 'Coffee details goes here', 2, 70, 1, '2019-10-24 00:31:00', 1, NULL, NULL),
(3, 'Strawberry Limeade', 'https://www.fifteenspatulas.com/wp-content/uploads/2015/07/Refreshing-Summer-Drinks-Fifteen-Spatulas-1-500x500.jpg', 'Drinks details goes here', 3, 80, 1, '2019-10-24 00:31:00', 0, NULL, NULL),
(4, 'Coffee 1', 'https://assets3.thrillist.com/v1/image/2797371/size/tmg-article_default_mobile.jpg', 'details', 4, 20, 1, '2019-10-24 21:20:00', 1, NULL, NULL),
(5, 'Salad 2', 'https://www.tasteofhome.com/wp-content/uploads/2017/10/exps6498_MRR133247D07_30_5b_WEB-2.jpg', 'Salad 2 Salad 2 Salad 2 Salad 2 Salad 2 Salad', 1, 20, 1, '2019-10-24 21:41:00', 1, NULL, NULL),
(6, 'gfhvbj', 'https://assets3.thrillist.com/v1/image/2797371/size/tmg-article_default_mobile.jpg', 'dfcghvb', 3, 33, 1, '2019-10-27 00:26:42', 0, NULL, NULL),
(7, 'cbvnbmn', 'https://assets3.thrillist.com/v1/image/2797371/size/tmg-article_default_mobile.jpg', 'cgfhvjbkn', 1, 345, 1, '2019-10-27 00:29:30', 0, NULL, NULL),
(8, 'asdasd', 'https://assets3.thrillist.com/v1/image/2797371/size/tmg-article_default_mobile.jpg', 'asdasd', 3, 324, 1, '2019-10-27 00:37:38', 0, NULL, NULL),
(9, 'gvhbn', 'https://assets3.thrillist.com/v1/image/2797371/size/tmg-article_default_mobile.jpg', 'fdcghvbj', 2, 77, 1, '2019-10-27 00:40:24', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `code`) VALUES
(1, 'Administrator', 'admin'),
(2, 'Waiter/Server', 'waiter'),
(3, 'Kitchen', 'kitchen'),
(4, 'Cashier', 'cashier');

-- --------------------------------------------------------

--
-- Table structure for table `statuses`
--

CREATE TABLE `statuses` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `statuses`
--

INSERT INTO `statuses` (`id`, `name`) VALUES
(1, 'Waiting kitchen to pick up'),
(2, 'Kitchen accepted'),
(3, 'Processing'),
(4, 'Ready to serve'),
(5, 'Pending payment'),
(6, 'Completed'),
(7, 'Cancel');

-- --------------------------------------------------------

--
-- Table structure for table `suborders`
--

CREATE TABLE `suborders` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `quantity` int(10) DEFAULT NULL,
  `remarks` text,
  `kitchen_remarks` text,
  `is_cancel` tinyint(1) DEFAULT '0',
  `date_created` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `suborders`
--

INSERT INTO `suborders` (`id`, `order_id`, `product_id`, `quantity`, `remarks`, `kitchen_remarks`, `is_cancel`, `date_created`) VALUES
(1, 9, 1, 3, '', NULL, 0, '2019-10-28 02:26:07'),
(2, 9, 2, 1, '', NULL, 0, '2019-10-28 02:26:22'),
(3, 9, 5, 1, '', NULL, 0, '2019-10-28 02:26:45'),
(4, 11, 2, 1, '30 minutes', NULL, 0, '2019-10-28 04:32:26');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `token_expires` datetime DEFAULT NULL,
  `api_token` varchar(255) DEFAULT NULL,
  `activation_date` datetime DEFAULT NULL,
  `secret` varchar(32) DEFAULT NULL,
  `secret_verified` tinyint(1) DEFAULT NULL,
  `tos_date` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `is_superuser` tinyint(1) NOT NULL DEFAULT '0',
  `role` varchar(255) DEFAULT 'user',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `additional_data` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `first_name`, `last_name`, `token`, `token_expires`, `api_token`, `activation_date`, `secret`, `secret_verified`, `tos_date`, `active`, `is_superuser`, `role`, `created`, `modified`, `additional_data`) VALUES
(1, 'superadmin', 'superadmin@example.com', '$2y$10$uFSYi1E/NM1YKvWdBjqssehqnDUV9PfJ7rgqB5NfDUizcyOACbJqe', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'superuser', '2019-10-26 23:28:11', '2019-10-26 23:28:11', NULL),
(2, 'waiter', NULL, '$2y$10$8BP2f0q07nlENEvncAYvmuVO1Ttj3XQFcmfGQL/9HY0QYgbWOzwQ6', 'Waiter', 'Waiter', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'waiter', '2019-10-26 23:28:55', '2019-10-26 23:38:59', NULL),
(3, 'kitchen', NULL, '$2y$10$iGv8X8KU/zcbGz/JGAg.xeZlc9UYzBGOw3BobJdkgG.uc8RgvTfeC', 'Kitchen', 'Kitchen', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'kitchen', '2019-10-26 23:43:00', '2019-10-26 23:43:00', NULL),
(4, 'cashier', NULL, '$2y$10$pns5DZg6.IkeS8UvzraNaO6y86pCSqSU4vXTauDF8al0IgVLEBHCO', 'Cashier', 'Cashier', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'cashier', '2019-10-26 23:43:24', '2019-10-26 23:43:24', NULL),
(5, 'admin', NULL, '$2y$10$mp1YmPIEFNrtD.vLHEm/BON19kuv8jIsSHNzXs.dsabdMHOCdxCMe', 'Admin', 'Admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'admin', '2019-10-26 23:43:44', '2019-10-26 23:43:44', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_temp`
--

CREATE TABLE `users_temp` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_temp`
--

INSERT INTO `users_temp` (`id`, `name`, `username`, `email`, `password`, `role_id`, `date_created`, `date_updated`) VALUES
(1, 'Cyrus Jayson', 'cyrusjayson', NULL, '12345678', 1, '2024-02-03 01:03:00', '2019-10-23 22:45:00'),
(2, 'cyrus 2', 'cyrus2', NULL, '1234567890', 2, NULL, '2019-10-23 23:07:45');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cake_d_c_users_phinxlog`
--
ALTER TABLE `cake_d_c_users_phinxlog`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status_id_idx` (`status_id`),
  ADD KEY `customer_id_idx` (`customer_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id_idx` (`category_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suborders`
--
ALTER TABLE `suborders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id_idx` (`product_id`),
  ADD KEY `order_id_idx` (`order_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_temp`
--
ALTER TABLE `users_temp`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_id_idx` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `statuses`
--
ALTER TABLE `statuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `suborders`
--
ALTER TABLE `suborders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users_temp`
--
ALTER TABLE `users_temp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `category_id` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `suborders`
--
ALTER TABLE `suborders`
  ADD CONSTRAINT `order_id` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `product_id` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users_temp`
--
ALTER TABLE `users_temp`
  ADD CONSTRAINT `role_id` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
